<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArtisteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Artiste::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */


    public function configureFields(string $pageName): iterable
    {

        $id = IdField::new('id')->hideOnForm();
        $nom = TextField::new('nom');
        $prenom = TextField::new('prenom');
        $photo = ImageField::new('photo')->setBasePath('assets/imgGroupes')->onlyOnIndex();
        $bio = TextEditorField::new('biographie')->hideOnIndex();

        return [$id, $nom, $prenom, $photo, $bio];
    }
}

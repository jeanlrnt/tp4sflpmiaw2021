<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return parent::index();
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();
        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
            return $action->setIcon('fa fa-edit')->setLabel(false);
        });

        $actions->update(Crud::PAGE_INDEX,Action::DELETE,function (Action $action) {
            return $action->setIcon('fa fa-trash-alt')->setLabel(false);
        });

        return $actions;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la Sirène');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoRoute('Retour au site','fa fa-sign-out-alt','homepage'),
            MenuItem::section('Admin', 'fa fa-cogs'),
            MenuItem::linkToCrud('Les concerts', 'fa fa-volume-up', Concert::class)->setController(ConcertCrudController::class),
            MenuItem::linkToCrud('Les concerts passés', 'fa fa-volume-down', Concert::class)->setController(ConcertPassesCrudController::class),
            MenuItem::linkToCrud('Les Artistes', 'fas fa-user-cog', Artiste::class),
            MenuItem::linkToCrud('Newsletter', 'fas fa-paper-plane', Email::class),
        ];
    }
}
